import { render, screen } from '@testing-library/react';
import App from './App';

test('renders soma link', () => {
  render(<App />);
  const linkElement = screen.getByText(/Soma/i);
  expect(linkElement).toBeInTheDocument();
});

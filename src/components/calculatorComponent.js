import React, { Component } from 'react';
import styled from 'styled-components';
import ButtonTeste from './Button/Button';

const CalculadoraContainer = styled.div`
    display:flex;
    flex-direction:column;
    width: 200px;
    border-radius: 8px;
    background: rgb(241, 243, 244) none repeat scroll 0% 0%;
    border: 1px solid transparent;
    margin: 0px 20px;
`;

const Display = styled.div`
    display:flex;
    justify-content: space-between;
`;

const Input = styled.input`
    margin-bottom: 10px;


`;

const Button = styled.button`
    background: none;
    border: 0px;
    color: #888;
    font-size: 15px;
    margin: 10px 0 0;
    font-family: Lato, sans-serif;
    cursor: pointer;
 
    &:hover {
        color: #333;
    }
`;

const Hr = styled.hr`
    display: flex;
    margin: 0%;
    color: rgb(221, 221, 221);
`;

const Label = styled.label`
    background-color: #00bfff;
    width:auto;
    text-align:right;
`;

const Div1 = styled.div`
   
    width: 60%;
    margin: 10px 20px ;  
    box-sizing: border-box;
    display: flex;
    flex-direction:column;
    flex-wrap: wrap;
    padding-top: 1rem;
`;

class Calculadora extends Component{

    soma() {
        let input1 = document.getElementById("x");
        let input2 = document.getElementById("y");

        let x = parseInt(input1.value);
        let y = parseInt(input2.value);

        let total = x + y;
        console.log(total);
        let label = document.getElementById("label");
        label.innerText = total;
        
    }

    sub(){
        let input1 = document.getElementById("x");
        let input2 = document.getElementById("y");

        let x = parseInt(input1.value);
        let y = parseInt(input2.value);

        let total = x - y;
        let label = document.getElementById("label");
        label.innerText = total;
    }

    multi(){
        let input1 = document.getElementById("x");
        let input2 = document.getElementById("y");

        let x = parseInt(input1.value);
        let y = parseInt(input2.value);

        let total = x * y;
        let label = document.getElementById("label");
        label.innerText = total;
    }

    div(){
        let input1 = document.getElementById("x");
        let input2 = document.getElementById("y");

        let x = parseInt(input1.value);
        let y = parseInt(input2.value);

        let total = x / y;
        let label = document.getElementById("label");
        label.innerText = total;
    }
    
    render(){
        return(
        <React.Fragment>
            <CalculadoraContainer>
                
                <Display>
                    <Div1>
                        <Input id="x" type="text" placeholder="valor 1"/>
                        <Input id="y" type="text" placeholder="valor 2"/>
                        <Label id="label" />
                    </Div1>
                </Display>
                
                <ButtonTeste onClick={this.soma} label={'Soma'}>
                </ButtonTeste>
                
                <Button onClick={this.sub}>
                    <Hr/>
                    Subtração
                    <Hr/>
                </Button>

                <Button onClick={this.multi}>
                <Hr/>
                    Multiplicação
                    <Hr/>
                </Button>

                <Button onClick={this.div}>
                    <Hr/>
                    Divisão
                    <Hr/>
                </Button>  
            </CalculadoraContainer>
        </React.Fragment>
        );
    }
}

export default Calculadora;
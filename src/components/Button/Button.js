import React from 'react';
import styled from 'styled-components';

const Button = styled.button`
    background: none;
    border: 0px;
    color: #888;
    font-size: 15px;
    margin: 10px 0 0;
    font-family: Lato, sans-serif;
    cursor: pointer;
 
    &:hover {
        color: #333;
    }
`;
const Hr = styled.hr`
    display: flex;
    margin: 0%;
    color: rgb(221, 221, 221);
`;

function ButtonTeste(props)
{
    return (
        <Button
            color={props.color}
            disabled={props.disabled} 
            size={props.size}
            variant={props.variant} 
            onClick={props.onClick}><Hr/>{props.label}<Hr/></Button>
    )
}

export default ButtonTeste;
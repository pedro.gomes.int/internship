import React from 'react';
import ButtonTeste from '../Button/Button';

export default {
    title: 'Button',
    argTypes: {
        label: { control: "text" },
        color:{
            control: { type: 'radio' },
            options: ['default', 'primary', 'secondary'],
        },
        size: {
            control: {
                type: 'radio',
                options: [ 'small', 'medium', 'large' ],
            }
        },
    },
};

export const Default = args => <ButtonTeste {...args} />
Default.args = {
    label: 'Sum',
    color: 'default',
    size: 'small',
};

export const Primary = args => <ButtonTeste {...args} />
Primary.args = {
    label: 'Sum',
    color: 'primary',
    size: 'medium',
};